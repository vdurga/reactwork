import React, {Component} from 'react';
import PropTypes from 'prop-types';
import classes from './Person.css';
import Auxiliary from '../../../hoc/Auxiliary';
import withClass from '../../../hoc/WithClass';
import AuthContext from '../../../context/auth-context';


class Person extends Component {
	constructor(props){
		super(props);
		this.inputElementRef = React.createRef();
	}
	
	static contextType = AuthContext;

	componentDidMount(){
		//this.inputElement.focus();
		this.inputElementRef.current.focus();
		console.log(this.context.authenticated);
	}
	
	render(){
		console.log('[Persons.js] rendering...');
		return (
			<Auxiliary>
			
			{this.context.authenticated ? <p> Authenticated! </p> : <p> PleaaseLogin </p>}
			
				<p key="i1" onClick={this.props.click}>
					I am {this.props.name} and I am {this.props.age} years old 
				</p>
				<p key="i2">
					{this.props.children}
				</p>
				<input 
					key="i3" 
					//ref={(inputEl) => {this.inputElement = inputEl}}
					ref= {this.inputElementRef}
					type ="text" 
					onChange={this.props.changed} 
					value={this.props.name}/>
		</Auxiliary>
		);
	}
}

Person.propTypes = {
	click: PropTypes.func,
	age: PropTypes.number,
	name: PropTypes.string,
	changed: PropTypes.func
};

export default withClass(Person, classes.Person);